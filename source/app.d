/*
 * with neomake
 *
 *  vim.g.neomake_sql_sqlfluff_maker = {
 *    exe = "sqlfluff-oneliner",
 *    args = {--dialect=…" },
 *    supports_stdin = 0,
 *    errorformat = 'f:%l:%c: %m,%f:%l:%c:%e:%k: %m',
 *    name = 'sqlfluff',
 *  }
 *
 * with efm-langserver
 *   sql = {
 *     {
 *       lintCommand = "sqlfluff-oneliner ${INPUT}",
 *       lintStdin = false,
 *       lintFormats = {
 *           'f:%l:%c: %m,%f:%l:%c:%e:%k: %m',
 *       },
 *       lintSource = "sqlfluff",
 *       lintIgnoreExitCode = true,
 *     }
 *   },
 */
import core.stdc.stdlib : exit;
import std.array : join;
import std.file : readText;
import std.json : parseJSON, JSONException, JSONValue;
import std.process : pipeProcess, Redirect, wait;
import std.stdio : writefln, stdout;
import std.range : appender;
import std.algorithm : copy;

void main(string[] args)
{
  auto sqlfluff_args = ["sqlfluff", "lint", "--format=json"];
  sqlfluff_args ~= args[1 .. $];
  auto sqlfluff = pipeProcess(sqlfluff_args, Redirect.stdout);
  auto sqlfluff_output_stream = appender!string;
  sqlfluff.stdout.byChunk(4096).copy(sqlfluff_output_stream);
  auto sqlf_output = sqlfluff_output_stream[];

  try
  {
    auto sqlf_errors = sqlf_output.parseJSON;
    foreach (e; sqlf_errors.array)
    {
      auto path = e["filepath"].str;
      foreach (v; e["violations"].array)
      {
        auto start_line_no = v["start_line_no"].integer;
        auto start_line_pos = v["start_line_pos"].integer;
        auto code = v["code"].str;
        auto description = v["description"].str;
        if (("end_line_no" in v) && ("end_line_pos" in v))
        {
          writefln("%s:%d:%d:%d:%d: (%s)%s", path, start_line_no, start_line_pos,
              v["end_line_no"].integer, v["end_line_pos"].integer, code, description);
        }
        else
        {
          writefln("%s:%d:%d: (%s)%s", path, start_line_no, start_line_pos, code, description);
        }
      }
    }
  }
  catch (JSONException e)
  {
    // cannot parse as json, probably some human-readable error
    // so let's print it as such
    stdout.writeln(sqlf_output);
  }
  finally
  {
    sqlfluff.pid.wait.exit;
  }

}

// vim: filetype=d expandtab tabstop=2 softtabstop=2 shiftwidth=2
